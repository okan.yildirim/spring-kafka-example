package com.okan.springkafkaexample.consumer;

import com.okan.springkafkaexample.AbstractKafkaTest;
import com.okan.springkafkaexample.TestProducerConfig;
import com.okan.springkafkaexample.config.KafkaProductConsumerConfiguration;
import com.okan.springkafkaexample.model.ProductMessage;
import com.okan.springkafkaexample.producer.ProductProducer;
import com.okan.springkafkaexample.service.DummyProductCreateService;
import org.junit.jupiter.api.Test;
import org.mockito.ArgumentCaptor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.kafka.KafkaAutoConfiguration;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.kafka.support.KafkaHeaders;
import org.springframework.messaging.Message;
import org.springframework.messaging.support.MessageBuilder;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.fail;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.timeout;
import static org.mockito.Mockito.verify;


@SpringBootTest(classes = {
        ProductConsumer.class,
        KafkaProductConsumerConfiguration.class,
        TestProducerConfig.class,
        KafkaAutoConfiguration.class,
})
class ProductConsumerTest extends AbstractKafkaTest {

    @MockBean
    DummyProductCreateService dummyProductCreateService;

    @MockBean
    ProductProducer productProducer;

    @Autowired
    KafkaTemplate<String, Object> kafkaTemplate;

    @Value("${spring.kafka.consumer.product.topic}")
    String topic;

    @Value("${spring.kafka.consumer.product-error.topic}")
    private String errorTopic;

    @Value("${spring.kafka.consumer.product-retry.topic}")
    private String retryTopic;

    @Test
    public void it_should_consume_product_message() {
        //given
        var productMessage = new ProductMessage();
        productMessage.id = 4074L;
        productMessage.price = 25D;
        productMessage.title = "shoes";
        //when
        kafkaTemplate.send(topic, productMessage.toString(), productMessage)
                .addCallback(result -> System.err.println("callback succ : " + result.toString()),
                        ex -> {
                            ex.printStackTrace();
                            fail(ex.getMessage());
                        });
        //then
        var productMessageArgumentCaptor = ArgumentCaptor.forClass(ProductMessage.class);

        verify(dummyProductCreateService, timeout(5000).atLeastOnce()).createProduct(productMessageArgumentCaptor.capture());
        var capturedUserMessage = productMessageArgumentCaptor.getValue();

        assertThat(capturedUserMessage).isEqualToComparingFieldByField(productMessage);
    }

    @Test
    public void it_should_send_to_manage_retry_when_exception_occurred() {
        //given
        var productMessage = new ProductMessage();
        productMessage.id = 4074L;
        productMessage.price = 25D;
        productMessage.title = "shoes";

        doThrow(RuntimeException.class).when(dummyProductCreateService).createProduct(any(ProductMessage.class));
        //when
        kafkaTemplate.send(topic, productMessage.toString(), productMessage)
                .addCallback(result -> System.err.println("callback succ : " + result.toString()),
                        ex -> {
                            ex.printStackTrace();
                            fail(ex.getMessage());
                        });

        //then
        var productMessageArgumentCaptor = ArgumentCaptor.forClass(ProductMessage.class);

        verify(productProducer, timeout(5000).atLeastOnce()).manageRetry(productMessageArgumentCaptor.capture(), eq(0));
        var capturedUserMessage = productMessageArgumentCaptor.getValue();

        assertThat(capturedUserMessage).isEqualToComparingFieldByField(productMessage);
    }

    @Test
    public void it_should_consume_retry_product_message() {
        //given
        var productMessage = new ProductMessage();
        productMessage.id = 4074L;
        productMessage.price = 25D;
        productMessage.title = "shoes";

        Message<ProductMessage> message = MessageBuilder
                .withPayload(productMessage)
                .setHeader(KafkaHeaders.TOPIC, retryTopic)
                .setHeader(KafkaHeaders.MESSAGE_KEY, productMessage.id.toString())
                .setHeader("RETRY_COUNT", 0)
                .build();
        //when
        kafkaTemplate.send(message)
                .addCallback(result -> System.err.println("callback succ : " + result.toString()),
                        ex -> {
                            ex.printStackTrace();
                            fail(ex.getMessage());
                        });
        //then
        var productMessageArgumentCaptor = ArgumentCaptor.forClass(ProductMessage.class);

        verify(dummyProductCreateService, timeout(5000).atLeastOnce()).createProduct(productMessageArgumentCaptor.capture());
        var capturedUserMessage = productMessageArgumentCaptor.getValue();

        assertThat(capturedUserMessage).isEqualToComparingFieldByField(productMessage);
    }

    @Test
    public void it_should_send_to_manage_retry_when_exception_occurred_on_retry() {
        //given
        var productMessage = new ProductMessage();
        productMessage.id = 4074L;
        productMessage.price = 25D;
        productMessage.title = "shoes";

        Message<ProductMessage> message = MessageBuilder
                .withPayload(productMessage)
                .setHeader(KafkaHeaders.TOPIC, retryTopic)
                .setHeader(KafkaHeaders.MESSAGE_KEY, productMessage.id.toString())
                .setHeader("RETRY_COUNT", 2)
                .build();

        doThrow(RuntimeException.class).when(dummyProductCreateService).createProduct(any(ProductMessage.class));
        //when
        kafkaTemplate.send(message)
                .addCallback(result -> System.err.println("callback succ : " + result.toString()),
                        ex -> {
                            ex.printStackTrace();
                            fail(ex.getMessage());
                        });

        //then
        var productMessageArgumentCaptor = ArgumentCaptor.forClass(ProductMessage.class);

        verify(productProducer, timeout(5000).atLeastOnce()).manageRetry(productMessageArgumentCaptor.capture(), eq(2));
        var capturedUserMessage = productMessageArgumentCaptor.getValue();

        assertThat(capturedUserMessage).isEqualToComparingFieldByField(productMessage);
    }

}