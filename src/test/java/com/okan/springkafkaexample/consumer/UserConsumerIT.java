package com.okan.springkafkaexample.consumer;

import com.okan.springkafkaexample.AbstractKafkaTest;
import com.okan.springkafkaexample.TestProducerConfig;
import com.okan.springkafkaexample.config.KafkaUserConsumerConfiguration;
import com.okan.springkafkaexample.model.UserMessage;
import com.okan.springkafkaexample.producer.UserProducer;
import com.okan.springkafkaexample.service.DummyUserEmailService;
import org.junit.jupiter.api.Test;
import org.mockito.ArgumentCaptor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.kafka.KafkaAutoConfiguration;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.kafka.core.KafkaTemplate;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.fail;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.timeout;
import static org.mockito.Mockito.verify;

@SpringBootTest(classes = {
        UserConsumer.class,
        KafkaUserConsumerConfiguration.class,
        TestProducerConfig.class,
        KafkaAutoConfiguration.class,

})
class UserConsumerIT extends AbstractKafkaTest {

    @MockBean
    DummyUserEmailService dummyUserEmailService;

    @MockBean
    UserProducer userProducer;

    @Autowired
    KafkaTemplate<String, Object> kafkaTemplate;

    @Value("${spring.kafka.consumer.user.topic}")
    String topic;

    @Value("${spring.kafka.producer.user.error-topic}")
    private String errorTopic;

    @Value("${spring.kafka.producer.user.retry-topic}")
    private String retryTopic;

    @Test
    public void it_should_consume_user_message() {
        //given
        var userMessage = new UserMessage();
        userMessage.setId(4074L);
        userMessage.setName("okan");
        userMessage.setSurname("yildirim");
        userMessage.setEmail("okan.yildirim@email.com");
        userMessage.setAge(25);
        //when
        kafkaTemplate.send(topic, userMessage.getId().toString(), userMessage)
                .addCallback(result -> System.err.println("callback succ : " + result.toString()),
                        ex -> {
                            ex.printStackTrace();
                            fail(ex.getMessage());
                        });
        //then
        var userMessageArgumentCaptor = ArgumentCaptor.forClass(UserMessage.class);

        verify(dummyUserEmailService, timeout(5000).atLeastOnce()).sendEmail(userMessageArgumentCaptor.capture());
        var capturedUserMessage = userMessageArgumentCaptor.getValue();

        assertThat(capturedUserMessage).isEqualToIgnoringNullFields(userMessage);
    }

    @Test
    public void it_should_send_error_topic_when_exception_occurred_consuming_message() {
        //given
        var userMessage = new UserMessage();
        userMessage.setId(4074L);
        userMessage.setName("okan");
        userMessage.setSurname("yildirim");
        userMessage.setEmail("okan.yildirim@email.com");
        userMessage.setAge(25);

        doThrow(RuntimeException.class).when(dummyUserEmailService).sendEmail(any(UserMessage.class));
        //when
        kafkaTemplate.send(topic, userMessage.getId().toString(), userMessage)
                .addCallback(result -> System.err.println("callback succ : " + result.toString()),
                        ex -> {
                            ex.printStackTrace();
                            fail(ex.getMessage());
                        });
        //then
        var userMessageArgumentCaptor = ArgumentCaptor.forClass(UserMessage.class);

        verify(userProducer, timeout(5000).atLeastOnce()).produceErrorMessage(userMessageArgumentCaptor.capture());
        var capturedUserMessage = userMessageArgumentCaptor.getValue();

        assertThat(capturedUserMessage).isEqualToComparingFieldByField(userMessage);
    }

    @Test
    public void it_should_consume_user_error_message() {
        //given
        var userMessage = new UserMessage();
        userMessage.setId(4074L);
        userMessage.setName("okan");
        userMessage.setSurname("yildirim");
        userMessage.setEmail("okan.yildirim@email.com");
        userMessage.setAge(25);
        userMessage.setErrorMessage("ErrorMessage");
        //when
        kafkaTemplate.send(errorTopic, userMessage.getId().toString(), userMessage)
                .addCallback(result -> System.err.println("callback succ : " + result.toString()),
                        ex -> {
                            ex.printStackTrace();
                            fail(ex.getMessage());
                        });
        //then
        var userMessageArgumentCaptor = ArgumentCaptor.forClass(UserMessage.class);

        verify(dummyUserEmailService, timeout(5000).atLeastOnce()).sendEmail(userMessageArgumentCaptor.capture());
        var capturedUserMessage = userMessageArgumentCaptor.getValue();

        assertThat(capturedUserMessage).isEqualToIgnoringGivenFields(userMessage,"errorMessage");
    }

    @Test
    public void it_should_resend_error_topic_when_exception_occurred_consuming_error_topic() {
        //given
        var userMessage = new UserMessage();
        userMessage.setId(4074L);
        userMessage.setName("okan");
        userMessage.setSurname("yildirim");
        userMessage.setEmail("okan.yildirim@email.com");
        userMessage.setAge(25);

        doThrow(RuntimeException.class).when(dummyUserEmailService).sendEmail(any(UserMessage.class));
        //when
        kafkaTemplate.send(errorTopic, userMessage.getId().toString(), userMessage)
                .addCallback(result -> System.err.println("callback succ : " + result.toString()),
                        ex -> {
                            ex.printStackTrace();
                            fail(ex.getMessage());
                        });
        //then
        var userMessageArgumentCaptor = ArgumentCaptor.forClass(UserMessage.class);

        verify(userProducer, timeout(5000).atLeastOnce()).produceErrorMessage(userMessageArgumentCaptor.capture());
        var capturedUserMessage = userMessageArgumentCaptor.getValue();

        assertThat(capturedUserMessage).isEqualToComparingFieldByField(userMessage);
    }

    @Test
    public void it_should_consume_user_retry_message() {
        //given
        var userMessage = new UserMessage();
        userMessage.setId(4074L);
        userMessage.setName("okan");
        userMessage.setSurname("yildirim");
        userMessage.setEmail("okan.yildirim@email.com");
        userMessage.setAge(25);
        userMessage.setErrorMessage("ErrorMessage");

        //when
        kafkaTemplate.send(retryTopic, userMessage.getId().toString(), userMessage)
                .addCallback(result -> System.err.println("callback succ : " + result.toString()),
                        ex -> {
                            ex.printStackTrace();
                            fail(ex.getMessage());
                        });
        //then
        var userMessageArgumentCaptor = ArgumentCaptor.forClass(UserMessage.class);

        verify(dummyUserEmailService, timeout(5000).atLeastOnce()).sendEmail(userMessageArgumentCaptor.capture());
        var capturedUserMessage = userMessageArgumentCaptor.getValue();

        assertThat(capturedUserMessage).isEqualToComparingFieldByField(userMessage);
    }

    @Test
    public void it_should_send_error_topic_when_exception_occurred_consuming_retry_topic() {
        //given
        var userMessage = new UserMessage();
        userMessage.setId(4074L);
        userMessage.setName("okan");
        userMessage.setSurname("yildirim");
        userMessage.setEmail("okan.yildirim@email.com");
        userMessage.setAge(25);

        doThrow(RuntimeException.class).when(dummyUserEmailService).sendEmail(any(UserMessage.class));
        //when
        kafkaTemplate.send(retryTopic, userMessage.getId().toString(), userMessage)
                .addCallback(result -> System.err.println("callback succ : " + result.toString()),
                        ex -> {
                            ex.printStackTrace();
                            fail(ex.getMessage());
                        });
        //then
        var userMessageArgumentCaptor = ArgumentCaptor.forClass(UserMessage.class);

        verify(userProducer, timeout(5000).atLeastOnce()).produceErrorMessage(userMessageArgumentCaptor.capture());
        var capturedUserMessage = userMessageArgumentCaptor.getValue();

        assertThat(capturedUserMessage).isEqualToComparingFieldByField(userMessage);
    }
}