package com.okan.springkafkaexample.producer;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.okan.springkafkaexample.AbstractKafkaTest;
import com.okan.springkafkaexample.config.KafkaProducerConfiguration;
import com.okan.springkafkaexample.model.UserMessage;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.kafka.core.DefaultKafkaConsumerFactory;
import org.springframework.kafka.listener.ContainerProperties;
import org.springframework.kafka.listener.KafkaMessageListenerContainer;
import org.springframework.kafka.listener.MessageListener;
import org.springframework.kafka.test.utils.ContainerTestUtils;
import org.springframework.kafka.test.utils.KafkaTestUtils;

import java.util.Map;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.TimeUnit;

import static org.assertj.core.api.Assertions.assertThat;

@SpringBootTest(classes = {
        UserProducer.class,
        KafkaProducerConfiguration.class,
})
class UserProducerIT extends AbstractKafkaTest {

    private static final Logger logger = LoggerFactory.getLogger(UserProducerIT.class);

    @Autowired
    private UserProducer userProducer;

    private KafkaMessageListenerContainer<String, UserMessage> listenerContainer;

    private BlockingQueue<ConsumerRecord<String, String>> consumerRecords;

    @Value("${spring.kafka.producer.user.topic}")
    String topic;

    @Value("${spring.kafka.producer.user.error-topic}")
    private String errorTopic;

    @Value("${spring.kafka.producer.user.retry-topic}")
    private String retryTopic;

    @BeforeEach
    void setUp() {
        consumerRecords = new LinkedBlockingQueue<>();

        ContainerProperties containerProperties = new ContainerProperties(topic, errorTopic, retryTopic);

        Map<String, Object> consumerProperties = KafkaTestUtils.consumerProps(
                "localhost:19092", "test", "true");

        DefaultKafkaConsumerFactory<String, UserMessage> consumer = new DefaultKafkaConsumerFactory<>(consumerProperties);

        listenerContainer = new KafkaMessageListenerContainer<>(consumer, containerProperties);
        listenerContainer.setupMessageListener((MessageListener<String, String>) record -> {
            logger.debug("Listened message='{}'", record.toString());
            consumerRecords.add(record);
        });
        listenerContainer.start();

        ContainerTestUtils.waitForAssignment(listenerContainer, 3);
    }

    @AfterEach
    public void tearDown() {
        listenerContainer.stop();
    }

    @Test
    public void it_should_produce_user_message() throws JsonProcessingException, InterruptedException {
        //given
        var userMessage = new UserMessage();
        userMessage.setId(4074L);
        userMessage.setName("okan");
        userMessage.setSurname("yildirim");
        userMessage.setEmail("okan.yildirim@email.com");
        userMessage.setAge(25);
        //when
        userProducer.produceMessage(userMessage);
        //then
        ConsumerRecord<String, String> received = consumerRecords.poll(10, TimeUnit.SECONDS.SECONDS);

        ObjectMapper mapper = new ObjectMapper();
        String json = mapper.writeValueAsString(userMessage);

        assertThat(received.value()).contains(json);
    }

    @Test
    public void it_should_produce_user_error_message() throws JsonProcessingException, InterruptedException {
        //given
        var userMessage = new UserMessage();
        userMessage.setId(4074L);
        userMessage.setName("okan");
        userMessage.setSurname("yildirim");
        userMessage.setEmail("okan.yildirim@email.com");
        userMessage.setAge(25);
        //when
        userProducer.produceErrorMessage(userMessage);
        //then
        ConsumerRecord<String, String> received = consumerRecords.poll(10, TimeUnit.SECONDS.SECONDS);

        ObjectMapper mapper = new ObjectMapper();
        String json = mapper.writeValueAsString(userMessage);

        assertThat(received.value()).contains(json);
    }

    @Test
    public void it_should_produce_user_retry_message() throws JsonProcessingException, InterruptedException {
        //given
        var userMessage = new UserMessage();
        userMessage.setId(4074L);
        userMessage.setName("okan");
        userMessage.setSurname("yildirim");
        userMessage.setEmail("okan.yildirim@email.com");
        userMessage.setAge(25);
        //when
        userProducer.produceRetryMessage(userMessage);
        //then
        ConsumerRecord<String, String> received = consumerRecords.poll(10, TimeUnit.SECONDS.SECONDS);

        ObjectMapper mapper = new ObjectMapper();
        String json = mapper.writeValueAsString(userMessage);

        assertThat(received.value()).contains(json);
    }
}