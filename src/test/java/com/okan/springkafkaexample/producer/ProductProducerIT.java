package com.okan.springkafkaexample.producer;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.okan.springkafkaexample.AbstractKafkaTest;
import com.okan.springkafkaexample.config.KafkaProducerConfiguration;
import com.okan.springkafkaexample.model.ProductMessage;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.kafka.core.DefaultKafkaConsumerFactory;
import org.springframework.kafka.listener.ContainerProperties;
import org.springframework.kafka.listener.KafkaMessageListenerContainer;
import org.springframework.kafka.listener.MessageListener;
import org.springframework.kafka.test.utils.ContainerTestUtils;
import org.springframework.kafka.test.utils.KafkaTestUtils;

import java.util.Map;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.TimeUnit;

import static org.assertj.core.api.Assertions.assertThat;

@SpringBootTest(classes = {
        ProductProducer.class,
        KafkaProducerConfiguration.class,
})
class ProductProducerIT extends AbstractKafkaTest {
    private static final Logger logger = LoggerFactory.getLogger(ProductProducerIT.class);

    @Autowired
    private ProductProducer productProducer;

    private KafkaMessageListenerContainer<String, ProductMessage> listenerContainer;

    private BlockingQueue<ConsumerRecord<String, String>> consumerRecords;

    @Value("${spring.kafka.producer.product.topic}")
    String topic;

    @Value("${spring.kafka.producer.product.error-topic}")
    private String errorTopic;

    @Value("${spring.kafka.producer.product.retry-topic}")
    private String retryTopic;

    @BeforeEach
    void setUp() {
        consumerRecords = new LinkedBlockingQueue<>();

        ContainerProperties containerProperties = new ContainerProperties(topic, errorTopic, retryTopic);

        Map<String, Object> consumerProperties = KafkaTestUtils.consumerProps(
                "localhost:19092", "test2", "true");

        DefaultKafkaConsumerFactory<String, ProductMessage> consumer = new DefaultKafkaConsumerFactory<>(consumerProperties);

        listenerContainer = new KafkaMessageListenerContainer<>(consumer, containerProperties);
        listenerContainer.setupMessageListener((MessageListener<String, String>) record -> {
            logger.debug("Listened message='{}'", record.toString());
            consumerRecords.add(record);
        });
        listenerContainer.start();

        ContainerTestUtils.waitForAssignment(listenerContainer, 3);
    }

    @AfterEach
    public void tearDown() {
        listenerContainer.stop();
    }

    @Test
    public void it_should_produce_product_message() throws JsonProcessingException, InterruptedException {
        //given
        var productMessage = new ProductMessage();
        productMessage.id = 4074L;
        productMessage.title = "title";
        productMessage.price = 10D;

        //when
        productProducer.produceMessage(productMessage);
        //then
        ConsumerRecord<String, String> received = consumerRecords.poll(10, TimeUnit.SECONDS.SECONDS);

        ObjectMapper mapper = new ObjectMapper();
        String json = mapper.writeValueAsString(productMessage);

        assertThat(received.value()).contains(json);
    }

    @Test
    public void it_should_manage_retry_product_message_when_retry_count_less_than_three() throws JsonProcessingException, InterruptedException {
        //given
        var productMessage = new ProductMessage();
        productMessage.id = 4074L;
        productMessage.title = "title";
        productMessage.price = 10D;

        //when
        productProducer.manageRetry(productMessage, 2);
        //then
        ConsumerRecord<String, String> received = consumerRecords.poll(10, TimeUnit.SECONDS.SECONDS);

        ObjectMapper mapper = new ObjectMapper();
        String json = mapper.writeValueAsString(productMessage);

        assertThat(received.value()).contains(json);
        assertThat(received.topic()).isEqualTo(retryTopic);

        received.headers().iterator().forEachRemaining(i -> {
            if (i.key().equalsIgnoreCase("RETRY_COUNT")) {
                assertThat(Integer.parseInt(new String(i.value()))).isEqualTo(Integer.valueOf(3));
            }
        });
    }

    @Test
    public void it_should_manage_retry_product_message_when_retry_count_is_three() throws JsonProcessingException, InterruptedException {
        //given
        var productMessage = new ProductMessage();
        productMessage.id = 4074L;
        productMessage.title = "title";
        productMessage.price = 10D;

        //when
        productProducer.manageRetry(productMessage, 3);
        //then
        ConsumerRecord<String, String> received = consumerRecords.poll(10, TimeUnit.SECONDS.SECONDS);

        ObjectMapper mapper = new ObjectMapper();
        String json = mapper.writeValueAsString(productMessage);

        assertThat(received.value()).contains(json);
        assertThat(received.topic()).isEqualTo(errorTopic);

        received.headers().iterator().forEachRemaining(i -> {
            if (i.key().equalsIgnoreCase("RETRY_COUNT")) {
                assertThat(Integer.parseInt(new String(i.value()))).isEqualTo(Integer.valueOf(4));
            }
        });
    }
}