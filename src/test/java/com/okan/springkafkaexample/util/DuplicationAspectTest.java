package com.okan.springkafkaexample.util;

import org.junit.jupiter.api.Test;
import org.springframework.aop.aspectj.annotation.AnnotationAwareAspectJAutoProxyCreator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.transaction.TransactionAutoConfiguration;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.annotation.Import;
import org.springframework.test.context.ContextConfiguration;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.catchThrowable;

@SpringBootTest(classes = {DuplicationAspect.class, DummyService.class})
@ContextConfiguration
@Import({AnnotationAwareAspectJAutoProxyCreator.class, TransactionAutoConfiguration.class})
class DuplicationAspectTest {

    @Autowired
    DummyService dummyService;

    @Test
    void it_should_catch_and_skip_exception() {
        Throwable throwable = catchThrowable(() -> dummyService.test());

        assertThat(throwable).isNull();
    }
}