package com.okan.springkafkaexample.util;

import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.sql.SQLException;

@Service
class DummyService {

    @Transactional
    @SkipDuplication
    public void test() {
        SQLException sqlException = new SQLException("reason", "23505");
        Exception cause = new Exception("test", sqlException);
        throw new DataIntegrityViolationException("test", cause);
    }
}