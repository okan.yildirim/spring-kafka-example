package com.okan.springkafkaexample;

import org.springframework.boot.test.util.TestPropertyValues;
import org.springframework.context.ApplicationContextInitializer;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.kafka.test.EmbeddedKafkaBroker;
import org.springframework.test.context.ContextConfiguration;

@ContextConfiguration(initializers = AbstractKafkaTest.Initializer.class)
public class AbstractKafkaTest {

    static EmbeddedKafkaBroker embeddedKafkaBroker;

    static {
        embeddedKafkaBroker = new EmbeddedKafkaBroker(1, true, 1)
                .kafkaPorts(19092)
                .zkPort(12881);

        embeddedKafkaBroker.afterPropertiesSet();
    }

    static class Initializer implements ApplicationContextInitializer<ConfigurableApplicationContext> {

        @Override
        public void initialize(ConfigurableApplicationContext configurableApplicationContext) {
            TestPropertyValues propertyValues = TestPropertyValues.of(
                    "spring.kafka.producer.bootstrap-servers=localhost:19092",
                    "spring.kafka.consumer.bootstrap-servers=localhost:19092"
            );
            propertyValues.applyTo(configurableApplicationContext);
        }
    }

}
