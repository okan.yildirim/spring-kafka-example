package com.okan.springkafkaexample;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.annotation.EnableAspectJAutoProxy;

@SpringBootTest
@EnableAspectJAutoProxy
class SpringKafkaExampleApplicationTests {

    @Test
    void contextLoads() {
    }

}
