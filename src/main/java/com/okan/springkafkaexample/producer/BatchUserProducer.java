package com.okan.springkafkaexample.producer;

import com.okan.springkafkaexample.model.UserMessage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.kafka.support.KafkaHeaders;
import org.springframework.messaging.Message;
import org.springframework.messaging.support.MessageBuilder;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class BatchUserProducer {

    private static final Logger logger = LoggerFactory.getLogger(BatchUserProducer.class);

    @Value("${spring.kafka.producer.batch-user.topic}")
    private String topic;

    private final KafkaTemplate<String, Object> kafkaTemplate;

    public BatchUserProducer(KafkaTemplate<String, Object> kafkaTemplate) {
        this.kafkaTemplate = kafkaTemplate;
    }

    public void produceBatchUserEvent(List<UserMessage> userMessages) {
        userMessages.forEach(this::produceUserEvent);
    }

    // Another type of producing kafka message
    private void produceUserEvent(UserMessage userMessage) {

        String key = String.valueOf(userMessage.getId());

        Message<UserMessage> kafkaMessage = MessageBuilder
                .withPayload(userMessage)
                .setHeader(KafkaHeaders.TOPIC, topic)
                .setHeader(KafkaHeaders.MESSAGE_KEY, key)
                .build();

        kafkaTemplate.send(kafkaMessage);
        logger.info("Producing userMessage: {}, to topic: {}, with key: {}", userMessage, topic, key);

    }
}
