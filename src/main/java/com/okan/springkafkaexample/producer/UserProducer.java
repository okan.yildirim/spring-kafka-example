package com.okan.springkafkaexample.producer;

import com.okan.springkafkaexample.model.UserMessage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Component;

@Component
public class UserProducer {

    private static final Logger logger = LoggerFactory.getLogger(UserProducer.class);

    @Value("${spring.kafka.producer.user.topic}")
    private String topic;

    @Value("${spring.kafka.producer.user.error-topic}")
    private String errorTopic;

    @Value("${spring.kafka.producer.user.retry-topic}")
    private String retryTopic;

    private final KafkaTemplate<String, Object> kafkaTemplate;

    public UserProducer(KafkaTemplate<String, Object> kafkaTemplate) {
        this.kafkaTemplate = kafkaTemplate;
    }

    public void produceMessage(UserMessage userMessage) {
        String key = String.valueOf(userMessage.getId());
        kafkaTemplate.send(topic, key, userMessage);
        logger.info("Producing userMessage: {}, to topic: {}, with key: {}", userMessage, topic, key);
    }

    public void produceErrorMessage(UserMessage userMessage) {
        String key = String.valueOf(userMessage.getId());
        kafkaTemplate.send(errorTopic, key, userMessage);
        logger.info("Producing userErrorMessage: {}, to topic: {}, with key: {}", userMessage, errorTopic, key);
    }

    public void produceRetryMessage(UserMessage userMessage) {
        String key = String.valueOf(userMessage.getId());
        kafkaTemplate.send(retryTopic, key, userMessage);
        logger.info("Producing userRetryMessage: {}, to topic: {}, with key: {}", userMessage, retryTopic, key);
    }
}
