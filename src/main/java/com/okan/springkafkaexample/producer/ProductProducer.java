package com.okan.springkafkaexample.producer;

import com.okan.springkafkaexample.model.ProductMessage;
import com.okan.springkafkaexample.model.UserMessage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.kafka.support.KafkaHeaders;
import org.springframework.messaging.Message;
import org.springframework.messaging.support.MessageBuilder;
import org.springframework.stereotype.Component;

@Component
public class ProductProducer {

    private static final int MAX_RETRY_COUNT = 3;
    private final Logger logger = LoggerFactory.getLogger(ProductProducer.class);

    @Value("${spring.kafka.producer.product.topic}")
    private String topic;

    @Value("${spring.kafka.producer.product.error-topic}")
    private String errorTopic;

    @Value("${spring.kafka.producer.product.retry-topic}")
    private String retryTopic;

    private final KafkaTemplate<String, Object> kafkaTemplate;

    public ProductProducer(KafkaTemplate<String, Object> kafkaTemplate) {
        this.kafkaTemplate = kafkaTemplate;
    }

    public void produceMessage(ProductMessage productMessage) {
        String key = String.valueOf(productMessage.id);
        kafkaTemplate.send(topic, key, productMessage);
        logger.info("Producing productMessage: {}, to topic: {}, with key: {}", productMessage, topic, key);
    }

    public void manageRetry(ProductMessage productMessage, int retryCount) {
        String topicName = retryCount < MAX_RETRY_COUNT ? retryTopic : errorTopic;

        String key = productMessage.id.toString();
        retryCount++;
        Message<ProductMessage> message = MessageBuilder
                .withPayload(productMessage)
                .setHeader(KafkaHeaders.TOPIC, topicName)
                .setHeader(KafkaHeaders.MESSAGE_KEY, key)
                .setHeader("RETRY_COUNT", retryCount)
                .build();

        kafkaTemplate.send(message);
        logger.info("Producing productMessage from retry manager: {}, to topic: {}, with key: {}, with retryCount: {}", productMessage, topicName, key, retryCount);
    }
}
