package com.okan.springkafkaexample.model;

import java.io.Serializable;

public class KafkaMessage implements Serializable {

    private String errorMessage;

    public String getErrorMessage() {
        return errorMessage;
    }

    public void setErrorMessage(String errorMessage) {
        this.errorMessage = errorMessage;
    }

    @Override
    public String toString() {
        return "KafkaMessage{" +
                "errorMessage='" + errorMessage + '\'' +
                '}';
    }
}
