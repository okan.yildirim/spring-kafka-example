package com.okan.springkafkaexample.model;

public class ProductMessage extends KafkaMessage {

    public Long id;
    public String title;
    public Double price;

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("ProductMessage{");
        sb.append("id=").append(id);
        sb.append(", title='").append(title).append('\'');
        sb.append(", price=").append(price);
        sb.append('}');
        return sb.toString();
    }
}
