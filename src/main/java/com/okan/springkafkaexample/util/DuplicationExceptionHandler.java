package com.okan.springkafkaexample.util;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.dao.DataIntegrityViolationException;

import java.sql.SQLException;

public class DuplicationExceptionHandler {

    private static final Logger logger = LoggerFactory.getLogger(DuplicationExceptionHandler.class);
    private static final String UNIQUE_CONSTRAINT_ERROR_CODE = "23505";

    public static void ignoreIfDuplication(Exception e) {
        if (e instanceof DataIntegrityViolationException) {
            SQLException exception = (SQLException) e.getCause().getCause();
            if (exception.getSQLState().equalsIgnoreCase(UNIQUE_CONSTRAINT_ERROR_CODE)) {
                logger.warn("Duplicated message. This message is skipping.", e);
            }
        }
    }
}
