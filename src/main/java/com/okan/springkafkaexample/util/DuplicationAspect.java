package com.okan.springkafkaexample.util;

import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.annotation.Order;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.stereotype.Component;

import java.sql.SQLException;

@Order(0)
@Aspect
@Component
public class DuplicationAspect {

    private static final String UNIQUE_CONSTRAINT_ERROR_CODE = "23505";
    private final Logger logger = LoggerFactory.getLogger(DuplicationAspect.class);

    @Around(value = "@annotation(com.okan.springkafkaexample.util.SkipDuplication)")
    public Object skipDuplicationError(ProceedingJoinPoint proceedingJoinPoint) throws Throwable {
        try {
            return proceedingJoinPoint.proceed();
        } catch (Exception ex) {
            if (isDuplicationException(ex)) {
                logger.warn("Duplication Error Occurred, skipping..", ex);
            } else {
                throw ex;
            }
        }
        return null;
    }

    public boolean isDuplicationException(Exception e) {
        if (e instanceof DataIntegrityViolationException) {
            if (e.getCause().getCause() instanceof SQLException) {
                SQLException exception = (SQLException) e.getCause().getCause();
                return exception.getSQLState().equalsIgnoreCase(UNIQUE_CONSTRAINT_ERROR_CODE);
            }
        }
        return false;
    }
}
