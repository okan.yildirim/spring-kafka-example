package com.okan.springkafkaexample.service;

import com.github.javafaker.Faker;
import com.okan.springkafkaexample.model.ProductMessage;
import com.okan.springkafkaexample.model.UserMessage;
import com.okan.springkafkaexample.producer.BatchUserProducer;
import com.okan.springkafkaexample.producer.ProductProducer;
import com.okan.springkafkaexample.producer.UserProducer;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.IntStream;

@Component
public class DummyMessageCreateJob {

    private static final int BATCH_SIZE = 500;

    private final UserProducer userProducer;
    private final BatchUserProducer batchUserProducer;
    private final ProductProducer productProducer;

    public DummyMessageCreateJob(UserProducer userProducer, BatchUserProducer batchUserProducer, ProductProducer productProducer) {
        this.userProducer = userProducer;
        this.batchUserProducer = batchUserProducer;
        this.productProducer = productProducer;
    }

    // TODO how to write test for scheduled job
    //@Scheduled(fixedRate = 10000)
    public void createUser() {

        UserMessage userMessage = getDummyUserMessage();

        userProducer.produceMessage(userMessage);
    }

    //@Scheduled(fixedRate = 5000)
    public void createBatchUser() {
        List<UserMessage> userMessages = new ArrayList<>();

        IntStream.range(0, BATCH_SIZE)
                .forEach(i -> userMessages.add(getDummyUserMessage()));

        batchUserProducer.produceBatchUserEvent(userMessages);

    }

    //@Scheduled(fixedRate = 1000)
    public void createProduct() {
        ProductMessage dummyProduct = getDummyProduct();
        productProducer.produceMessage(dummyProduct);
    }

    private UserMessage getDummyUserMessage() {
        Faker faker = new Faker();
        String firstName = faker.name().firstName();
        String surname = faker.name().lastName();
        String email = firstName.toLowerCase() + "." + surname.toLowerCase() + "@email.com";

        UserMessage userMessage = new UserMessage();
        userMessage.setId(faker.number().numberBetween(0L, 999999L));
        userMessage.setName(firstName);
        userMessage.setSurname(surname);
        userMessage.setEmail(email);
        userMessage.setAge(faker.number().numberBetween(18, 90));
        return userMessage;
    }

    private ProductMessage getDummyProduct() {
        Faker faker = new Faker();
        ProductMessage productMessage = new ProductMessage();
        productMessage.id = faker.number().numberBetween(0L, 999999L);
        productMessage.price = faker.number().randomDouble(2, 5, 1000);
        productMessage.title = faker.food().dish();
        return productMessage;
    }
}
