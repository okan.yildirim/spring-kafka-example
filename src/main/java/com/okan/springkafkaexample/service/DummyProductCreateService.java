package com.okan.springkafkaexample.service;

import com.okan.springkafkaexample.model.ProductMessage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

@Service
public class DummyProductCreateService {

    private final Logger logger = LoggerFactory.getLogger(DummyProductCreateService.class);

    public void createProduct(ProductMessage productMessage) {

        if (productMessage.price > 900D){
            throw new IllegalArgumentException("Price must be less than 900");
        }
        logger.info("Product created for {},", productMessage);
    }
}
