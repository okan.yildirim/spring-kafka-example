package com.okan.springkafkaexample.service;

import com.okan.springkafkaexample.model.UserMessage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

@Service
public class DummyUserEmailService {

    private final Logger logger = LoggerFactory.getLogger(DummyUserEmailService.class);

    public void sendEmail(UserMessage userMessage){
        logger.info("Email send for {},", userMessage.getEmail());
    }
}
