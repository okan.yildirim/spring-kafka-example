package com.okan.springkafkaexample.config;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.okan.springkafkaexample.model.UserMessage;
import org.apache.kafka.clients.consumer.ConsumerConfig;
import org.apache.kafka.common.serialization.StringDeserializer;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.kafka.config.ConcurrentKafkaListenerContainerFactory;
import org.springframework.kafka.core.ConsumerFactory;
import org.springframework.kafka.core.DefaultKafkaConsumerFactory;
import org.springframework.kafka.support.converter.DefaultJackson2JavaTypeMapper;
import org.springframework.kafka.support.converter.Jackson2JavaTypeMapper;
import org.springframework.kafka.support.serializer.ErrorHandlingDeserializer2;
import org.springframework.kafka.support.serializer.JsonDeserializer;

import java.util.HashMap;
import java.util.Map;

@Configuration
public class KafkaUserConsumerConfiguration {

    @Value("${spring.kafka.consumer.bootstrap-servers}")
    private String bootstrapServers;

    @Value("${spring.kafka.consumer.user.auto-offset-reset}")
    private String autoOffsetReset;

    @Bean
    ConcurrentKafkaListenerContainerFactory<String, UserMessage> defaultKafkaListenerContainerFactory() {
        var factory = new ConcurrentKafkaListenerContainerFactory<String, UserMessage>();
        factory.setConsumerFactory(kafkaConsumerFactory());
        factory.setErrorHandler(new KafkaDeserializationErrorHandler());
        return factory;
    }

    @Bean
    ConsumerFactory<String, UserMessage> kafkaConsumerFactory() {
        var typeMapper = new DefaultJackson2JavaTypeMapper();
        typeMapper.setTypePrecedence(Jackson2JavaTypeMapper.TypePrecedence.INFERRED);

        var objectMapper = new ObjectMapper().configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
        var keyDeserializer = new ErrorHandlingDeserializer2<>(new StringDeserializer());
        var jsonDeserializer = new JsonDeserializer<>(UserMessage.class, objectMapper);
        jsonDeserializer.setTypeMapper(typeMapper);

        var valueDeserializer = new ErrorHandlingDeserializer2<>(jsonDeserializer);
        return new DefaultKafkaConsumerFactory<>(kafkaConsumerConfigs(), keyDeserializer, valueDeserializer);
    }

    Map<String, Object> kafkaConsumerConfigs() {
        Map<String, Object> props = new HashMap<>();
        props.put(ConsumerConfig.BOOTSTRAP_SERVERS_CONFIG, bootstrapServers);
        props.put(ConsumerConfig.KEY_DESERIALIZER_CLASS_CONFIG, StringDeserializer.class);
        props.put(ConsumerConfig.VALUE_DESERIALIZER_CLASS_CONFIG, JsonDeserializer.class);
        props.put(ConsumerConfig.AUTO_OFFSET_RESET_CONFIG, autoOffsetReset);
        props.put(ConsumerConfig.MAX_POLL_RECORDS_CONFIG, 100); // Default 500
        //props.put(ConsumerConfig.MAX_POLL_INTERVAL_MS_CONFIG, 300000);
        //props.put(ConsumerConfig.REQUEST_TIMEOUT_MS_CONFIG, 40000);
        //props.put(ConsumerConfig.SESSION_TIMEOUT_MS_CONFIG, 10000);
        return props;
    }
}
