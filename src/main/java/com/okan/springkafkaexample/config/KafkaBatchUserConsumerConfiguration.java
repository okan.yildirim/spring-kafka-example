package com.okan.springkafkaexample.config;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.okan.springkafkaexample.model.UserMessage;
import org.apache.kafka.clients.consumer.ConsumerConfig;
import org.apache.kafka.common.serialization.StringDeserializer;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.kafka.config.ConcurrentKafkaListenerContainerFactory;
import org.springframework.kafka.core.ConsumerFactory;
import org.springframework.kafka.core.DefaultKafkaConsumerFactory;
import org.springframework.kafka.listener.ContainerProperties;
import org.springframework.kafka.support.converter.DefaultJackson2JavaTypeMapper;
import org.springframework.kafka.support.converter.Jackson2JavaTypeMapper;
import org.springframework.kafka.support.serializer.ErrorHandlingDeserializer2;
import org.springframework.kafka.support.serializer.JsonDeserializer;

import java.util.HashMap;
import java.util.Map;

@Configuration
public class KafkaBatchUserConsumerConfiguration {

    @Value("${spring.kafka.consumer.bootstrap-servers}")
    private String bootstrapServers;

    @Value("${spring.kafka.consumer.user.auto-offset-reset}")
    private String autoOffsetReset;

    @Bean
    ConcurrentKafkaListenerContainerFactory<String, UserMessage> batchUserKafkaListenerContainerFactory() {
        var factory = new ConcurrentKafkaListenerContainerFactory<String, UserMessage>();
        factory.setConsumerFactory(batchUserKafkaConsumerFactory());
        factory.getContainerProperties().setAckMode(ContainerProperties.AckMode.BATCH);
        // factory.getContainerProperties().setSyncCommits(true);
        factory.setBatchListener(Boolean.TRUE);
        return factory;
    }

    @Bean
    ConsumerFactory<String, UserMessage> batchUserKafkaConsumerFactory() {
        var typeMapper = new DefaultJackson2JavaTypeMapper();
        typeMapper.setTypePrecedence(Jackson2JavaTypeMapper.TypePrecedence.INFERRED);

        var objectMapper = new ObjectMapper().configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
        var keyDeserializer = new ErrorHandlingDeserializer2<>(new StringDeserializer());
        var jsonDeserializer = new JsonDeserializer<>(UserMessage.class, objectMapper);
        jsonDeserializer.setTypeMapper(typeMapper);

        var valueDeserializer = new ErrorHandlingDeserializer2<>(jsonDeserializer);
        return new DefaultKafkaConsumerFactory<>(kafkaConsumerConfigs(), keyDeserializer, valueDeserializer);
    }

    Map<String, Object> kafkaConsumerConfigs() {
        Map<String, Object> props = new HashMap<>();
        props.put(ConsumerConfig.BOOTSTRAP_SERVERS_CONFIG, bootstrapServers);
        props.put(ConsumerConfig.KEY_DESERIALIZER_CLASS_CONFIG, StringDeserializer.class);
        props.put(ConsumerConfig.VALUE_DESERIALIZER_CLASS_CONFIG, JsonDeserializer.class);
        props.put(ConsumerConfig.AUTO_OFFSET_RESET_CONFIG, autoOffsetReset);
        //props.put(ConsumerConfig.MAX_POLL_RECORDS_CONFIG, 1000);
        return props;
    }
}
