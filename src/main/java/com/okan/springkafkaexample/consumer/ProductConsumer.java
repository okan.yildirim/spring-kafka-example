package com.okan.springkafkaexample.consumer;

import com.okan.springkafkaexample.model.ProductMessage;
import com.okan.springkafkaexample.producer.ProductProducer;
import com.okan.springkafkaexample.service.DummyProductCreateService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.kafka.support.KafkaHeaders;
import org.springframework.messaging.handler.annotation.Header;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.stereotype.Component;

@Component
public class ProductConsumer {

    private final Logger logger = LoggerFactory.getLogger(ProductConsumer.class);

    private final DummyProductCreateService dummyProductCreateService;
    private final ProductProducer productProducer;

    public ProductConsumer(DummyProductCreateService dummyProductCreateService, ProductProducer productProducer) {
        this.dummyProductCreateService = dummyProductCreateService;
        this.productProducer = productProducer;
    }

    @KafkaListener(
            topics = "${spring.kafka.consumer.product.topic}",
            groupId = "${spring.kafka.consumer.group-id}",
            containerFactory = "productKafkaListenerContainerFactory",
            autoStartup = "${spring.kafka.consumer.product.enable}"
    )
    public void consumeProduct(@Payload ProductMessage productMessage,
                               @Header(KafkaHeaders.RECEIVED_PARTITION_ID) Integer partition,
                               @Header(KafkaHeaders.OFFSET) Long offset,
                               @Header(KafkaHeaders.RECEIVED_TOPIC) String topic) {

        try {
            logger.info("Consumed product message with topic: {}, and partition: {}, and offset: {}, {}", topic, partition, offset, productMessage);
            dummyProductCreateService.createProduct(productMessage);
        } catch (Exception e) {
            productMessage.setErrorMessage(e.getMessage());
            productProducer.manageRetry(productMessage, 0);
            logger.error("Error occurred when consuming product message: {}, ", productMessage, e);
        }
    }

    @KafkaListener(
            topics = "${spring.kafka.consumer.product-retry.topic}",
            groupId = "${spring.kafka.consumer.group-id}",
            containerFactory = "productKafkaListenerContainerFactory",
            autoStartup = "${spring.kafka.consumer.product-retry.enable}"
    )
    public void consumeRetryMessage(@Payload ProductMessage productMessage,
                                    @Header(KafkaHeaders.RECEIVED_PARTITION_ID) Integer partition,
                                    @Header(KafkaHeaders.OFFSET) Long offset,
                                    @Header(KafkaHeaders.RECEIVED_TOPIC) String topic,
                                    @Header("RETRY_COUNT") Integer retryCount) {

        try {
            logger.info("Consumed product retry message:{} with topic: {}, and partition: {}, and offset: {}, and retry {}",
                    productMessage, topic, partition, offset, retryCount);
            dummyProductCreateService.createProduct(productMessage);
        } catch (Exception e) {
            productMessage.setErrorMessage(e.getMessage());
            productProducer.manageRetry(productMessage, retryCount);
            logger.error("Error occurred when consuming product retry message: {}, ", productMessage, e);
        }
    }
}
