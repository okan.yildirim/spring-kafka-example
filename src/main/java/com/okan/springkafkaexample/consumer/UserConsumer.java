package com.okan.springkafkaexample.consumer;

import com.okan.springkafkaexample.model.UserMessage;
import com.okan.springkafkaexample.producer.UserProducer;
import com.okan.springkafkaexample.service.DummyUserEmailService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.kafka.support.KafkaHeaders;
import org.springframework.messaging.handler.annotation.Header;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.stereotype.Component;

@Component
public class UserConsumer {

    private final Logger logger = LoggerFactory.getLogger(UserConsumer.class);

    private final DummyUserEmailService dummyUserEmailService;
    private final UserProducer userProducer;

    public UserConsumer(DummyUserEmailService dummyUserEmailService,
                        UserProducer userProducer) {
        this.dummyUserEmailService = dummyUserEmailService;
        this.userProducer = userProducer;
    }

    @KafkaListener(
            topics = "${spring.kafka.consumer.user.topic}",
            groupId = "${spring.kafka.consumer.group-id}",
            containerFactory = "defaultKafkaListenerContainerFactory",
            autoStartup = "${spring.kafka.consumer.user.enable}"
    )
    public void consumeUser(@Payload UserMessage userMessage,
                            @Header(KafkaHeaders.RECEIVED_PARTITION_ID) Integer partition,
                            @Header(KafkaHeaders.OFFSET) Long offset,
                            @Header(KafkaHeaders.RECEIVED_TOPIC) String topic) {

        try {
            logger.info("Consumed user message with topic: {}, and partition: {}, and offset: {}, {}", topic, partition, offset, userMessage);
            dummyUserEmailService.sendEmail(userMessage);
        } catch (Exception e) {
            userMessage.setErrorMessage(e.getMessage());
            userProducer.produceErrorMessage(userMessage);
            logger.error("Error occurred when consuming user message: {}, ", userMessage, e);
        }
    }


    @KafkaListener(
            topics = "${spring.kafka.consumer.user-error.topic}",
            groupId = "${spring.kafka.consumer.group-id}",
            containerFactory = "defaultKafkaListenerContainerFactory",
            autoStartup = "${spring.kafka.consumer.user-error.enable}"
    )
    public void consumeErrorMessage(@Payload UserMessage userMessage,
                            @Header(KafkaHeaders.RECEIVED_PARTITION_ID) Integer partition,
                            @Header(KafkaHeaders.OFFSET) Long offset,
                            @Header(KafkaHeaders.RECEIVED_TOPIC) String topic) {

        try {
            logger.info("Consumed user error message with topic: {}, and partition: {}, and offset: {}, {}", topic, partition, offset, userMessage);
            dummyUserEmailService.sendEmail(userMessage);
        } catch (Exception e) {
            userMessage.setErrorMessage(e.getMessage());
            userProducer.produceErrorMessage(userMessage);
            logger.error("Error occurred when consuming user error message: {}, ", userMessage, e);
        }
    }

    @KafkaListener(
            topics = "${spring.kafka.consumer.user-retry.topic}",
            groupId = "${spring.kafka.consumer.group-id}",
            containerFactory = "defaultKafkaListenerContainerFactory",
            autoStartup = "${spring.kafka.consumer.user-retry.enable}"
    )
    public void consumeRetryMessage(@Payload UserMessage userMessage,
                                    @Header(KafkaHeaders.RECEIVED_PARTITION_ID) Integer partition,
                                    @Header(KafkaHeaders.OFFSET) Long offset,
                                    @Header(KafkaHeaders.RECEIVED_TOPIC) String topic) {

        try {
            logger.info("Consumed user retry message with topic: {}, and partition: {}, and offset: {}, {}", topic, partition, offset, userMessage);
            dummyUserEmailService.sendEmail(userMessage);
        } catch (Exception e) {
            userMessage.setErrorMessage(e.getMessage());
            userProducer.produceErrorMessage(userMessage);
            logger.error("Error occurred when consuming user retry message: {}, ", userMessage, e);
        }
    }
}
