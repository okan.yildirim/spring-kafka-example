package com.okan.springkafkaexample.consumer;

import com.okan.springkafkaexample.model.UserMessage;
import com.okan.springkafkaexample.service.DummyUserEmailService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.kafka.support.KafkaHeaders;
import org.springframework.messaging.handler.annotation.Header;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class BatchUserConsumer {

    private final Logger logger = LoggerFactory.getLogger(BatchUserConsumer.class);

    private final DummyUserEmailService dummyUserEmailService;

    public BatchUserConsumer(DummyUserEmailService dummyUserEmailService) {
        this.dummyUserEmailService = dummyUserEmailService;
    }

    @KafkaListener(
            topics = "${spring.kafka.consumer.batch-user.topic}",
            groupId = "${spring.kafka.consumer.group-id}",
            containerFactory = "batchUserKafkaListenerContainerFactory",
            autoStartup = "${spring.kafka.consumer.batch-user.enable}"
    )
    public void consumeUser(@Payload List<UserMessage> userMessages,
                            @Header(KafkaHeaders.RECEIVED_PARTITION_ID) Integer partition,
                            @Header(KafkaHeaders.OFFSET) Long offset,
                            @Header(KafkaHeaders.RECEIVED_TOPIC) String topic) {

        try {
            logger.info("Consumed batch user message with topic: {}, and partition: {}, and offset: {}, {}", topic, partition, offset, userMessages.get(0));
            userMessages.forEach(dummyUserEmailService::sendEmail);
        } catch (Exception e) {
            logger.error("Error occurred when consuming user messages, ", e);
        }
    }
}
